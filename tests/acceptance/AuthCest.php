<?php


class AuthCest
{

    /**
     * @var \Helper\DbHelper
     */
    protected $db;

    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    protected function _inject(\Helper\DbHelper $db)
    {
        $this->db = $db;
    }

    // tests
    public function singUpTest(AcceptanceTester $I)
    {
        $I->wantTo('Sign up on site');
        $I->amOnPage('/register');
        $I->fillField('fos_user_registration_form[username]', 'TestUser');
        $I->fillField('fos_user_registration_form[email]', 'test@aa.aa');
        $I->fillField('fos_user_registration_form[plainPassword][first]', '12345');
        $I->fillField('fos_user_registration_form[plainPassword][second]', '12345');
        $I->selectOption('fos_user_registration_form[gender]', 'male');
        $I->fillField('fos_user_registration_form[city]', 'Jakarta');
        $I->click('signupSubmit');
        $I->seeCurrentUrlEquals('/register/confirmed');
    }

    /**
     * @depends singUpTest
     */
    public function logInTest(AcceptanceTester $I)
    {
        $I->wantTo('Log into site');
        $I->amOnPage('/login');
        $I->fillField('_username', 'TestUser');
        $I->fillField('_password', '12345');
        $I->click('_submit');
        $I->seeCurrentUrlEquals('/');
        $this->db->deleteFromDatabase('user', ['username'=>'TestUser']);
    }
}
