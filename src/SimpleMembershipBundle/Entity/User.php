<?php

namespace SimpleMembershipBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="SimpleMembershipBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    const USER_GRENDER = [
        'male' => 'Male',
        'female' => 'Female',
    ];

    const USER_PREMIUM_LIST = [
        '0' => 'Disabled',
        '1' => 'Enabled',
    ];

    const USER_PREMIUM_TERM = [
        '0' => 'Disable',
        '3' => '3 Month',
        '6' => '6 Month',
        '12' => '12 Month',
    ];
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @Assert\NotBlank(message="Please enter your name.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The name is too short.",
     *     maxMessage="The name is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $username;
    /**
     * @ORM\Column(name="blog", type="string", length=255, nullable=true)
     */
    protected $blog;
    /**
     * @ORM\Column(name="first_name", type="text", length=128)
     */
    private $firstName;
    /**
     * @ORM\Column(name="last_name", type="text", length=128)
     */
    private $lastName;
    /**
     * @ORM\Column(name="birthday", type="date")
     */
    private $birthday;
    /**
     * @ORM\Column(name="occupation", type="text", length=128)
     */
    private $occupation;
    /**
     * @ORM\Column(name="phone", type="string", length=20)
     */
    private $phone;
    /**
     * @ORM\Column(name="gender", type="string", length=6, nullable=false)
     * @Assert\NotBlank(message="Please make your choice.", groups={"Registration", "Profile"})
     */
    private $gender;
    /**
     * @ORM\Column(name="city", type="string", length=128, nullable=true)
     * @Assert\NotBlank(message="Please enter your city.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The city is too short.",
     *     maxMessage="The city is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    private $city;
    /**
     * @ORM\OneToMany(
     *     targetEntity="SimpleMembershipBundle\Entity\Payment", mappedBy="user", cascade={"remove", "persist"}
     *     )
     */
    private $payments;
    /**
     * @var boolean
     * @ORM\Column(name="is_premium", type="boolean")
     */
    private $isPremium;

    /**
     * @var \DateTime
     * @ORM\Column(name="premium_expired", type="datetime")
     */
    private $premiumExpired;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="SimpleMembershipBundle\Entity\Product", inversedBy="users", cascade={"persist", "detach"}
     *     )
     * @ORM\JoinTable(name="user_products")
     */
    private $products;

    /**
     * @ORM\OneToMany(
     *     targetEntity="SimpleMembershipBundle\Entity\Checkout", mappedBy="user", cascade={"remove", "persist"}
     *     )
     * @ORM\JoinTable(name="users_products")
     */
    private $orders;

    /**
     * @var \DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(
     *     targetEntity="SimpleMembershipBundle\Entity\Complaint", mappedBy="user", cascade={"remove", "persist"}
     *     )
     */
    private $complaints;

    public function __construct()
    {
        parent::__construct();
        $this->createdAt = new \DateTime();
        $this->premiumExpired = new \DateTime('1970-1-1 00:00:00');
        $this->isPremium = false;
        $this->gender = self::USER_GRENDER['male'];
        $this->products = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->complaints = new ArrayCollection();
    }

    public function getGrender()
    {
        return self::USER_GRENDER;
    }

    public function getPremiumList()
    {
        return self::USER_GRENDER;
    }

    public function getPremiumTerm()
    {
        return self::USER_GRENDER;
    }

    public function __toString()
    {
        return $this->getUsername();
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * @param mixed $occupation
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * @param mixed $blog
     */
    public function setBlog($blog)
    {
        $this->blog = $blog;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @param Payment $payment
     * @return User
     */
    public function setPayments($payment)
    {
        $this->payments = $payment;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return boolean
     */
    public function isPremium()
    {
        return $this->isPremium;
    }

    /**
     * @param boolean $isPremium
     */
    public function setIsPremium($isPremium)
    {
        $this->isPremium = $isPremium;
    }

    /**
     * @return \DateTime
     */
    public function getPremiumExpired()
    {
        return $this->premiumExpired;
    }

    /**
     * @param \DateTime $premiumExpired
     * @return User
     */
    public function setPremiumExpired($premiumExpired)
    {
        $this->premiumExpired = $premiumExpired;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return mixed
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param mixed $orders
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     * @return User
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getComplaints()
    {
        return $this->complaints;
    }

    /**
     * @param mixed $complaints
     * @return User
     */
    public function setComplaints($complaints)
    {
        $this->complaints = $complaints;

        return $this;
    }

    /**
     * @param Complaint $complaint
     * @return User
     */
    public function addChapter(Complaint $complaint)
    {
        $this->complaints[] = $complaint;

        return $this;
    }
}
