<?php

namespace SimpleMembershipBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Post
 *
 * @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="SimpleMembershipBundle\Repository\PostRepository")
 */
class Post
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="smallint")
     */
    private $sort;

    /**
     * @ORM\Column(name="document", type="string", nullable=true)
     */
    private $document;

    /**
     * @ORM\Column(name="pdf_thumb", type="string", nullable=true)
     */
    private $pdfThumb;

    /**
     * For upload
     */
    private $pdfThumbFile;

    /**
     * @var string
     * @ORM\Column(name="youtube", type="string", length=128, nullable=true)
     */
    private $youtube;

    /**
     * @var string
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @Gedmo\Mapping\Annotation\Slug(
     *     fields={"title"},
     *     unique=true,
     *     updatable=false,
     *     separator="-"
     * )
     * @ORM\Column(name="slug")
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity="SimpleMembershipBundle\Entity\Product", inversedBy="posts")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="SimpleMembershipBundle\Entity\Chapter", inversedBy="posts")
     * @ORM\JoinColumn(name="chapter_id", referencedColumnName="id")
     */
    private $chapter;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param integer $sort
     * @return Post
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param $document
     * @return $this
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return Post
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return string
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * @param string $youtube
     * @return Post
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;

        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return Post
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPdfThumb()
    {
        return $this->pdfThumb;
    }

    /**
     * @param mixed $pdfThumb
     * @return Post
     */
    public function setPdfThumb($pdfThumb)
    {
        $this->pdfThumb = $pdfThumb;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPdfThumbFile()
    {
        return $this->pdfThumbFile;
    }

    /**
     * @param mixed $pdfThumbFile
     * @return Post
     */
    public function setPdfThumbFile($pdfThumbFile)
    {
        $this->pdfThumbFile = $pdfThumbFile;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return Post
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChapter()
    {
        return $this->chapter;
    }

    /**
     * @param mixed $chapter
     * @return Post
     */
    public function setChapter($chapter)
    {
        $this->chapter = $chapter;

        return $this;
    }
}
