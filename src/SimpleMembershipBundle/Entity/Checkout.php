<?php

namespace SimpleMembershipBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;

/**
 * Checkout
 *
 * @ORM\Table(name="checkout")
 * @ORM\Entity(repositoryClass="SimpleMembershipBundle\Repository\CheckoutRepository")
 */
class Checkout
{
    const ORDER_STATUS_NEW = 'new';
    const ORDER_STATUS_PENDING = 'pending';
    const ORDER_STATUS_DONE = 'done';
    const STATUS_LIST = [
        'New' => self::ORDER_STATUS_NEW,
        'Pending' => self::ORDER_STATUS_PENDING,
        'Done' => self::ORDER_STATUS_DONE,
    ];
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var
     * @ORM\ManyToMany(targetEntity="SimpleMembershipBundle\Entity\Product", mappedBy="orders", cascade={"persist", "detach"})
     */
    private $products;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="SimpleMembershipBundle\Entity\User", inversedBy="orders", cascade={"persist", "detach"})
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(name="status", type="string", length=16, nullable=true)
     */
    private $status;

    /**
     * Checkout constructor.
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->status = self::ORDER_STATUS_NEW;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Product
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     * @return Checkout
     */
    public function setProducts($products)
    {
        $this->products = $products;

        return $this;
    }

    /**
     * @param Product $product
     * @return Checkout
     */
    public function addProduct(Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Checkout
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Checkout
     */
    public function setStatus($status)
    {
        if(!in_array($status, self::STATUS_LIST)) {
            throw new ParameterNotFoundException($status);
        }
        $this->status = $status;

        return $this;
    }
}
