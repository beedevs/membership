<?php

namespace SimpleMembershipBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="SimpleMembershipBundle\Repository\ProductRepository")
 */
class Product
{

    const PRODUCT_LEVEL_BEGINNER = 'beginner';
    const PRODUCT_LEVEL_INTERMEDIATE = 'intermediate';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, unique=true)
     */
    private $title;

    /**
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\Column(name="is_free", type="boolean")
     */
    private $isFree;

    /**
     * @ORM\Column(name="thumb", type="string", nullable=true)
     */
    private $thumb;

    private $thumbFile;

    /**
     * @ORM\OneToMany(
     *     targetEntity="SimpleMembershipBundle\Entity\Post", mappedBy="product", cascade={"persist", "detach"}
     *     )
     */
    private $posts;

    /**
     * @ORM\OneToMany(
     *     targetEntity="SimpleMembershipBundle\Entity\Chapter", mappedBy="product", cascade={"persist", "detach"}
     *     )
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $chapter;

    /**
     * @ORM\ManyToMany(targetEntity="SimpleMembershipBundle\Entity\User", mappedBy="products")
     */
    private $users;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="SimpleMembershipBundle\Entity\Category", inversedBy="products"
     *     )
     * @ORM\JoinTable(name="category_product")
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity="SimpleMembershipBundle\Entity\Checkout", inversedBy="products")
     * @ORM\JoinTable(name="ordered_products",
    joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
    inverseJoinColumns={@ORM\JoinColumn(name="checkout_id", referencedColumnName="id")}
     * )
     */
    private $orders;

    /**
     * @ORM\Column(name="level", type="string", length=64)
     */
    private $level;

    /**
     * @ORM\Column(name="author", type="string", length=128)
     */
    private $author;

    /**
     * @ORM\Column(name="length", type="smallint")
     */
    private $length;

    /**
     * @ORM\Column(name="is_published", type="boolean")
     */
    private $isPublished;

    /**
     * @ORM\Column(name="published", type="date")
     */
    private $published;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->isFree = false;
        $this->posts = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->chapter = new ArrayCollection();
        $this->published = new \DateTime();
        $this->isPublished = true;
    }

    public static function getProductLevels()
    {
        return [
            'Beginner' => self::PRODUCT_LEVEL_BEGINNER ,
            'Intermediate' => self::PRODUCT_LEVEL_INTERMEDIATE,
        ];
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsFree()
    {
        return $this->isFree;
    }

    /**
     * @param mixed $isFree
     * @return Product
     */
    public function setIsFree($isFree)
    {
        $this->isFree = $isFree;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getThumb()
    {
        return $this->thumb;
    }

    /**
     * @param mixed $thumb
     * @return Product
     */
    public function setThumb($thumb)
    {
        $this->thumb = $thumb;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param Post[] $posts
     * @return Product
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $users
     * @return Product
     */
    public function setUsers($users)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param mixed $orders
     * @return Product
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;

        return $this;
    }

    /**
     * @param Checkout $order
     * @return Product
     */
    public function addOrder(Checkout $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getThumbFile()
    {
        return $this->thumbFile;
    }

    /**
     * @param mixed $thumbFile
     * @return Product
     */
    public function setThumbFile($thumbFile)
    {
        $this->thumbFile = $thumbFile;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     * @return Product
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     * @return Product
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param mixed $length
     * @return Product
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * @param mixed $isPublished
     * @return Product
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param mixed $published
     * @return Product
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return Product
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChapter()
    {
        return $this->chapter;
    }

    /**
     * @param mixed $chapter
     * @return Product
     */
    public function setChapter($chapter)
    {
        $this->chapter = $chapter;

        return $this;
    }

    /**
     * @param Chapter $chapter
     * @return Product
     */
    public function addChapter(Chapter $chapter)
    {
        $this->chapter[] = $chapter;

        return $this;
    }


}
