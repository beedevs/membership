<?php

namespace SimpleMembershipBundle\Form;

use SimpleMembershipBundle\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description', TextareaType::class)
            ->add('thumb', HiddenType::class)
            ->add('thumbFile',
                FileType::class,
                [
                    'required' => false
                ]
            )
            ->add(
                'isFree',
                ChoiceType::class,
                [
                    'choices' => [
                        'Paid' => 0,
                        'Free' => 1,
                    ],
                ]
            )
            ->add(
                'level',
                ChoiceType::class,
                ['choices' => Product::getProductLevels(),]
            )
            ->add('published', DateType::class)
            ->add('author', TextType::class)
            ->add('length', IntegerType::class)
            ->add(
                'category',
                EntityType::class,
                [
                    'class' => 'SimpleMembershipBundle:Category',
                    'choice_label' => 'title',
                    'multiple' => false
                ]
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'SimpleMembershipBundle\Entity\Product',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'simplemembershipbundle_product';
    }


}
