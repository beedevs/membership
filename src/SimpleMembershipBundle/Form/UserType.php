<?php

namespace SimpleMembershipBundle\Form;

use Doctrine\ORM\EntityRepository;
use SimpleMembershipBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'locked',
            ChoiceType::class,
            [
                'choices' => [
                    'Active' => 0,
                    'Banned' => 1,
                ],
            ]
        );

        $builder->add(
            'premium_term',
            ChoiceType::class,
            [
                'choices' => array_flip(User::USER_PREMIUM_TERM),
                'mapped' => false
            ]
        );

        $builder->add(
            'products',
            EntityType::class,
            [
                'class' => 'SimpleMembershipBundle:Product',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->where('p.isFree = :false')
                        ->setParameter('false', false)
                        ->orderBy('p.title', 'ASC');
                },
                'choice_label' => 'title',
                'multiple' => true,
                'expanded' => true
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'SimpleMembershipBundle\Entity\User',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'simplemembershipbundle_user';
    }
}
