<?php

namespace SimpleMembershipBundle\Form;

use SimpleMembershipBundle\Entity\Payment;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaymentUserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'bankName',
                EntityType::class,
                [
                    'class' => 'SimpleMembershipBundle:Bank',
                    'choice_label' => 'Title',
                ]
            )
            ->add(
                'paymentDate',
                DateType::class,
                [
                    'data' => new \DateTime(),
                ]
            )
            ->add(
                'description',
                TextareaType::class
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'SimpleMembershipBundle\Entity\Payment',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'simplemembershipbundle_payments';
    }


}
