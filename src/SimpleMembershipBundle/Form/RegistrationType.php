<?php
/**
 * Created by PhpStorm.
 * User: moris
 * Date: 27/11/2016
 * Time: 13:48
 */

namespace SimpleMembershipBundle\Form;

use SimpleMembershipBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $date = new \DateTime();
        $minDate = clone($date);
        $maxDate = clone($date);
        $builder->add('first_name', TextType::class);
        $builder->add('last_name', TextType::class);
        $builder->add('username', TextType::class);
        $builder->add('email', EmailType::class);
        $builder->add('gender', ChoiceType::class, ['choices' => array_flip(User::USER_GRENDER)]);
        $builder->add('birthday', DateType::class, [
            'years' => range(
                $minDate->modify('-90 years')->format('Y'),
                $maxDate->modify('-16 years')->format('Y'),
                1
            ),
        ]);
        $builder->add('occupation', TextType::class);
        $builder->add('phone', TextType::class);
        $builder->add('blog', TextType::class);
        $builder->add('city', TextType::class);
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'SimpleMembershipBundle\Entity\User',
            )
        );
    }
}