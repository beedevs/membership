<?php

namespace SimpleMembershipBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title')
            ->add('sort')
            ->add(
                'document',
                FileType::class,
                [
                    'required' => false,
                    'data_class' => null,
                ]
            )
            ->add('pdfThumb', HiddenType::class)
            ->add('pdfThumbFile',
                FileType::class,
                [
                    'required' => false
                ]
            )
            ->add(
                'youtube',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'text',
                TextareaType::class,
                [
                    'required' => false,
                    'attr' => [
                        'class' => 'tinymce'
                    ]
                ]
            )

            ->add(
                'product',
                EntityType::class,
                [
                    'class' => 'SimpleMembershipBundle:Product',
                    'choice_label' => 'title',
                ]
            )
            ->add(
                'chapter',
                EntityType::class,
                [
                    'class' => 'SimpleMembershipBundle:Chapter',
                    'choice_label' => 'title',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'SimpleMembershipBundle\Entity\Post',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'simplemembershipbundle_post';
    }
}
