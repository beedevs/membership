<?php

namespace SimpleMembershipBundle\Twig\Extension;

/**
 * Class JsonDecodeExtension
 * @package SimpleMembershipBundle\Twig\Extension
 */
class JsonDecodeExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('json_decode', [$this, 'getJsonDecode']),
        ];
    }


    /**
     * @param $string
     * @param bool $asscoiative
     * @return mixed
     */
    public function getJsonDecode($string, $asscoiative = true)
    {
        $data = json_decode($string, true);

        if (json_last_error() == 0) {
            return $data;
        }

        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'json_decode';
    }
}
