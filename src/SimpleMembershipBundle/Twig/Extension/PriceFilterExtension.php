<?php

namespace SimpleMembershipBundle\Twig\Extension;

use Doctrine\ORM\EntityManager;

class PriceFilterExtension extends \Twig_Extension
{

    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('get_price_filter', [$this, 'priceFilter']),
        ];
    }

    /**
     * @return array
     */
    public function priceFilter()
    {
        $prices = $this->entityManager->getRepository('SimpleMembershipBundle:Product')->getDistintcPriceTypes();

        return $prices;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'get_price_filter';
    }
}
