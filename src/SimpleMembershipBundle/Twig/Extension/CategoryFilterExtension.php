<?php

namespace SimpleMembershipBundle\Twig\Extension;

use Doctrine\ORM\EntityManager;

class CategoryFilterExtension extends \Twig_Extension
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('get_categories_filter', [$this, 'categoryFilter']),
        ];
    }

    /**
     * @return array
     */
    public function categoryFilter()
    {
        $categories = $this->entityManager->getRepository('SimpleMembershipBundle:Category')->getCategoriesFilter();

        return $categories;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'get_categories_filter';
    }
}
