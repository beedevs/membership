<?php

namespace SimpleMembershipBundle\Twig\Extension;

class TwigDateDiffExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('date_diff', [$this, 'TwigDateDiff']),
        ];
    }

    public function TwigDateDiff(\DateTime $dateTime, $format = 'd')
    {
        $now = new \DateTime();
        $diff = $now->diff($dateTime);
        return $diff->format($format);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'date_diff';
    }
}
