<?php

namespace SimpleMembershipBundle\Twig\Extension;

use SimpleMembershipBundle\Entity\Product;

class LevelFilterExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('get_level_filter', [$this, 'levelFilter']),
        ];
    }

    /**
     * @return array
     */
    public function levelFilter()
    {
        return array_flip(Product::getProductLevels());
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'get_level_filter';
    }
}
