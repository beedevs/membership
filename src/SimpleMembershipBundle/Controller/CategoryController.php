<?php
/**
 * Created by PhpStorm.
 * User: ader
 * Date: 2/5/2017
 * Time: 11:27 PM
 */

namespace SimpleMembershipBundle\Controller;

use SimpleMembershipBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('SimpleMembershipBundle:Category')->findAll();

        $form = $this->createForm('SimpleMembershipBundle\Form\CategoryType', null, array(
            'action' => $this->generateUrl('admin_category_new')
        ));

        return $this->render(
            '@SimpleMembership/category/index.html.twig',
            array(
                'categories' => $categories,
                'form' => $form->createView()
            )
        );
    }

    public function newAction(Request $request) {
        $category = new Category();
        $form = $this->createForm('SimpleMembershipBundle\Form\CategoryType', $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('admin_category_index');
        }
    }

    public function editAction(Request $request, Category $category)
    {
        $editForm = $this->createForm('SimpleMembershipBundle\Form\CategoryType', $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->persist($category);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_category_index');
        }

        return $this->render(
            '@SimpleMembership/category/edit.html.twig',
            array(
                'category' => $category,
                'form' => $editForm->createView()
            )
        );
    }

    public function deleteAction(Category $category)
    {
        if (!$category) {
            throw $this->createNotFoundException('No category found');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        return new Response('ok');
    }

}