<?php

namespace SimpleMembershipBundle\Controller;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use SimpleMembershipBundle\Entity\Complaint;
use SimpleMembershipBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $productRepository = $em->getRepository('SimpleMembershipBundle:Product');
        $query = $productRepository->preBuildQuery();

        if ($request->getMethod() == "POST") {
            $this->get('sm.filters.ilab')->setFilters($request, $query);
        }

        $products = $query->getQuery()->getResult();

        return $this->render(
            'SimpleMembershipBundle::ilab.html.twig',
            [
                'products' => $products,
            ]
        );
    }

    public function dashboardAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.change_password.form.factory');

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var $userManager UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_profile_show');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(
                FOSUserEvents::CHANGE_PASSWORD_COMPLETED,
                new FilterUserResponseEvent($user, $request, $response)
            );

            return $response;
        }

        $complaintForm = $this->createForm(
            'SimpleMembershipBundle\Form\ComplaintType',
            null,
            array(
                'action' => $this->generateUrl('simple_membership_complaint'),
            )
        );

        return $this->render(
            'SimpleMembershipBundle::dashboard.html.twig',
            array(
                'form' => $form->createView(),
                'complaint_form' => $complaintForm->createView(),
            )
        );
    }

    public function detailsAction(Post $post)
    {
        return $this->render(
            'SimpleMembershipBundle::detail-materi-active.html.twig',
            [
                'post' => $post,
            ]
        );
    }

    public function complaintAction(Request $request)
    {
        $complaint = new Complaint();
        $form = $this->createForm('SimpleMembershipBundle\Form\ComplaintType', $complaint);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $complaint->setUser($this->getUser());
            $em->persist($complaint);
            $em->flush();

            $this->sendComplaintNotification($complaint);

            return $this->redirectToRoute('simple_membership_dashboard');
        }
    }

    private function sendComplaintNotification(Complaint $complaint)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('New Complaint')
            ->setFrom($this->getParameter('sender_email'))
            ->setTo($this->getParameter('sender_email'))
            ->setBody(
                $this->renderView(
                    '@SimpleMembership/Emails/complaint.html.twig',
                    [
                        'complaint' => $complaint,
                    ]
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
    }
}
