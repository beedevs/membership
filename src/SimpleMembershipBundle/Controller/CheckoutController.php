<?php

namespace SimpleMembershipBundle\Controller;

use SimpleMembershipBundle\Entity\Checkout;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CheckoutController extends Controller
{
    public function indexAction()
    {
        $entityManager = $this->getDoctrine()->getEntityManager();

        $orders = $entityManager->getRepository('SimpleMembershipBundle:Checkout')->getCheckoutList();

        return $this->render(
            '@SimpleMembership/checkout/index.html.twig',
            [
                'orders' => $orders,
            ]
        );
    }

    public function orderAction($orderId)
    {
        $entityManager = $this->getDoctrine()->getEntityManager();
        $order = $entityManager->getRepository('SimpleMembershipBundle:Checkout')->findOneBy(['id' => $orderId]);

        if (!$order) {
            throw new NotFoundHttpException();
        }

        return $this->render(
            '@SimpleMembership/checkout/view.html.twig',
            [
                'order' => $order,
            ]
        );
    }

    public function submitAction($orderId)
    {
        $entityManager = $this->getDoctrine()->getEntityManager();
        $order = $entityManager->getRepository('SimpleMembershipBundle:Checkout')->findOneBy(['id' => $orderId]);

        if (!$order) {
            throw new NotFoundHttpException();
        }

        $user = $order->getUser();
        $products = $order->getProducts();

        $user->setProducts($products);

        $order->setStatus(Checkout::ORDER_STATUS_DONE);
        $entityManager->persist($user);
        $entityManager->persist($order);
        $entityManager->flush();
        $this->get('session')->getFlashBag()->add('success', 'Order submited');

        return $this->redirect($this->generateUrl('simple_membership_checkout_index'));
    }
}
