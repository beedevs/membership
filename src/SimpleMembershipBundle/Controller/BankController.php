<?php

namespace SimpleMembershipBundle\Controller;

use SimpleMembershipBundle\Entity\Bank;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Bank controller.
 *
 */
class BankController extends Controller
{
    /**
     * Lists all bank entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $banks = $em->getRepository('SimpleMembershipBundle:Bank')->findAll();

        return $this->render(
            '@SimpleMembership/bank/index.html.twig',
            array(
                'banks' => $banks,
            )
        );
    }

    /**
     * Creates a new bank entity.
     *
     */
    public function newAction(Request $request)
    {
        $bank = new Bank();
        $form = $this->createForm('SimpleMembershipBundle\Form\BankType', $bank);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bank);
            $em->flush($bank);

            return $this->redirectToRoute('admin_bank_show', array('id' => $bank->getId()));
        }

        return $this->render(
            '@SimpleMembership/bank/new.html.twig',
            array(
                'bank' => $bank,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a bank entity.
     *
     */
    public function showAction(Bank $bank)
    {
        $deleteForm = $this->createDeleteForm($bank);

        return $this->render(
            '@SimpleMembership/bank/show.html.twig',
            array(
                'bank' => $bank,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing bank entity.
     *
     */
    public function editAction(Request $request, Bank $bank)
    {
        $deleteForm = $this->createDeleteForm($bank);
        $editForm = $this->createForm('SimpleMembershipBundle\Form\BankType', $bank);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_bank_edit', array('id' => $bank->getId()));
        }

        return $this->render(
            '@SimpleMembership/bank/edit.html.twig',
            array(
                'bank' => $bank,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a bank entity.
     *
     */
    public function deleteAction(Request $request, Bank $bank)
    {
        $form = $this->createDeleteForm($bank);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($bank);
            $em->flush($bank);
        }

        return $this->redirectToRoute('admin_bank_index');
    }

    /**
     * Creates a form to delete a bank entity.
     *
     * @param Bank $bank The bank entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Bank $bank)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_bank_delete', array('id' => $bank->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
