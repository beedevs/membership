<?php

namespace SimpleMembershipBundle\Controller;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use SimpleMembershipBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * User controller.
 *
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('SimpleMembershipBundle:User')->findAll();

        return $this->render(
            '@SimpleMembership/user/index.html.twig',
            array(
                'users' => $users,
            )
        );
    }

    /**
     * Finds and displays a user entity.
     *
     */
    public function showAction(User $user)
    {
        $age = (new \DateTime())->diff($user->getBirthday())->format('%y');
        return $this->render(
            '@SimpleMembership/user/show.html.twig',
            array(
                'age' => $age,
                'user' => $user,
            )
        );
    }

    public function editAction(Request $request, User $user)
    {
        $editForm = $this->createForm('SimpleMembershipBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $premiumTerm = $editForm->get('premium_term')->getData();
            $premiumEndDate = new \DateTime('now');

            if ($premiumTerm != 0) {
                if ($user->isPremium() && $user->getPremiumExpired() > $premiumEndDate) {
                    $premiumEndDate = $user->getPremiumExpired()->modify("+ $premiumTerm month");
                } else {
                    $premiumEndDate->modify("+ $premiumTerm month");
                }

                $user->setPremiumExpired($premiumEndDate);
            }

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_user_show', array('id' => $user->getId()));
        }

        $age = (new \DateTime())->diff($user->getBirthday())->format('%y');

        return $this->render(
            '@SimpleMembership/user/edit.html.twig',
            array(
                'age' => $age,
                'user' => $user,
                'form' => $editForm->createView(),
            )
        );
    }

    public function deleteAction(User $user)
    {
        if (!$user) {
            throw $this->createNotFoundException('No user found');
        }

        $em = $this->getDoctrine()->getManager();
        try {
            $em->remove($user);
            $em->flush();
        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add('error', 'Unable to remove this user : ' . $e->getMessage());
        }


        return $this->redirect($this->generateUrl('admin_user_index'));
    }
}
