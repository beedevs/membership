<?php

namespace SimpleMembershipBundle\Controller;

use Doctrine\Common\Collections\Criteria;
use SimpleMembershipBundle\Entity\Payment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Payment controller.
 *
 */
class PaymentController extends Controller
{
    /**
     * Lists all payment entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $paymentRepository = $em->getRepository('SimpleMembershipBundle:Payment');

        if ($this->isGranted('ROLE_ADMIN')) {
            $payments = $paymentRepository->findAll();
        } else {
            $payments = $paymentRepository->findby(
                ['user' => $this->getUser()],
                ['id' => Criteria::ASC]
            );
        }

        return $this->render(
            '@SimpleMembership/payments/index.html.twig',
            array(
                'payments' => $payments,
            )
        );
    }

    /**
     * Creates a new payment entity.
     *
     */
    public function newAction(Request $request)
    {
        $payment = new Payment();

        if ($this->isGranted('ROLE_ADMIN')) {
            $form = $this->createForm('SimpleMembershipBundle\Form\PaymentsType', $payment);
        } else {
            $form = $this->createForm('SimpleMembershipBundle\Form\PaymentUserType', $payment);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if (!$this->isGranted('ROLE_ADMIN')) {
                $payment->setUser($this->getUser());
            }
            $em->persist($payment);
            $em->flush($payment);

            if ($this->isGranted('ROLE_ADMIN')) {
                return $this->redirectToRoute('payments_index');
            } else {
                return $this->redirectToRoute('simple_membership_homepage');
            }

        }

        return $this->render(
            '@SimpleMembership/payments/new.html.twig',
            array(
                'payment' => $payment,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a payment entity.
     *
     */
    public function showAction(Payment $payment)
    {
        $deleteForm = $this->createDeleteForm($payment);

        return $this->render(
            '@SimpleMembership/payments/show.html.twig',
            array(
                'payment' => $payment,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Creates a form to delete a payment entity.
     *
     * @param Payment $payment The payment entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Payment $payment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_payments_delete', array('id' => $payment->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a form to edit an existing payment entity.
     *
     */
    public function editAction(Request $request, Payment $payment)
    {
        $deleteForm = $this->createDeleteForm($payment);
        $editForm = $this->createForm('SimpleMembershipBundle\Form\PaymentsType', $payment);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_payments_edit', array('id' => $payment->getId()));
        }

        return $this->render(
            '@SimpleMembership/payments/edit.html.twig',
            array(
                'payment' => $payment,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a payment entity.
     *
     */
    public function deleteAction(Request $request, Payment $payment)
    {
        $form = $this->createDeleteForm($payment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($payment);
            $em->flush($payment);
        }

        return $this->redirectToRoute('payments_index');
    }
}
