<?php

namespace SimpleMembershipBundle\Controller;

use SimpleMembershipBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Post controller.
 *
 */
class PostController extends Controller
{
    /**
     * Lists all post entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $posts = $em->getRepository('SimpleMembershipBundle:Post')->findAll();

        return $this->render(
            '@SimpleMembership/post/index.html.twig',
            array(
                'posts' => $posts,
            )
        );
    }

    /**
     * Creates a new post entity.
     *
     */
    public function newAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm('SimpleMembershipBundle\Form\PostType', $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$post->getText() && !$post->getDocument() && !$post->getYoutube()) {
                $this->get('session')->getFlashBag()->add('danger', 'Field up on of fields - Text, Youtube,PDF');

                return $this->redirect($this->generateUrl('admin_post_new'));
            }

            $em = $this->getDoctrine()->getManager();
            $this->pdfFileUpload($post);
            $this->pdfThumbFileUpload($post);
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('admin_post_index');
        }

        return $this->render(
            '@SimpleMembership/post/new.html.twig',
            array(
                'post' => $post,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param Post $post
     */
    private function pdfFileUpload($post)
    {
        $document = $post->getDocument();

        if ($document) {
            $docFileName = md5(uniqid()).'.'.$document->guessExtension();
            $path = $this->getParameter('media_folder');

            $document->move(
                $path.'/pdf/',
                $docFileName
            );
            $post->setDocument($docFileName);
        }
    }

    /**
     * @param Post $post
     */
    private function pdfThumbFileUpload($post)
    {
        $pdfThumb = $post->getPdfThumbFile();

        if ($pdfThumb) {
            $pdfThumbFileName = md5(uniqid()).'.'.$pdfThumb->guessExtension();
            $path = $this->getParameter('media_folder');

            $pdfThumb->move(
                $path.'/img/',
                $pdfThumbFileName
            );
            $post->setPdfThumb($pdfThumbFileName);
        }
    }

    /**
     * Displays a form to edit an existing post entity.
     *
     */
    public function editAction(Request $request, Post $post)
    {
        $editForm = $this->createForm('SimpleMembershipBundle\Form\PostType', $post);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->pdfFileUpload($post);
            $this->pdfThumbFileUpload($post);
            $this->getDoctrine()->getManager()->persist($post);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_post_index');
        }

        return $this->render(
            '@SimpleMembership/post/edit.html.twig',
            array(
                'post' => $post,
                'form' => $editForm->createView(),
            )
        );
    }

    /**
     * Deletes a post entity.
     *
     */
    public function deleteAction(Post $post)
    {
        if (!$post) {
            throw $this->createNotFoundException('No post found');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();

        return new Response('ok');
    }
}
