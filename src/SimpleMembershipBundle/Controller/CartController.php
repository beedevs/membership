<?php

namespace SimpleMembershipBundle\Controller;

use SimpleMembershipBundle\Entity\Checkout;
use SimpleMembershipBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CartController extends Controller
{
    public function indexAction()
    {
        return $this->render('', []);
    }

    public function addProductAction($productId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository('SimpleMembershipBundle:Product')->find($productId);

        if (!$product) {
            throw new NotFoundHttpException('Product not found');
        }

        $cart = [];

        $session = $this->get('session');
        if ($session->has('cart')) {
            $cart = json_decode($session->get('cart'), true);
        }

        $cart[$productId] = [
            'productId' => $productId,
            'title' => $product->getTitle(),
        ];

        $session->set('cart', json_encode($cart));
        $session->getFlashBag()->add('success', 'Product added to cart');

        return new JsonResponse(
            [
                'status' => 'success',
                'data' => "Product {$product->getTitle()} added",
            ]
        );
    }

    public function checkoutAction()
    {
        if (!$this->get('session')->has('cart')) {
            throw new \Exception('Cart is empty');
        }

        $flashMessage = $this->get('session')->getFlashBag();

        $entityManager = $this->getDoctrine()->getManager();
        $cart = json_decode($this->get('session')->get('cart'), true);

        if (!$cart) {
            $flashMessage->add('warning', "Please choice product(s) ");

            return $this->redirect($this->generateUrl('product_index'));
        }

        $order = $entityManager->getRepository('SimpleMembershipBundle:Checkout')->addCheckout(
            $cart,
            $this->getUser()
        );

        $success = false;

        if (!$order) {
            $flashMessage->add('danger', "We can't proceed your order please in current moment.");
        } else {
            $success = true;
            $flashMessage->add('success', 'Product(s) was added, our TEAM will get in touch with you for a details');
        }

        $this->sendOrder($this->getUser(), $order, $success);

        $this->get('session')->remove('cart');

        return $this->redirect($this->generateUrl('product_index'));
    }

    /**
     * @param User $user
     * @param Checkout $checkout
     * @param boolean $status
     */
    private function sendOrder(User $user, Checkout $checkout, $status)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('New order')
            ->setFrom($this->getParameter('sender_email'))
            ->setTo($this->getParameter('sender_email'))
            ->setBody(
                $this->renderView(
                    '@SimpleMembership/Emails/order.html.twig',
                    [
                        'userName' => $user->getUsername(),
                        'orderId' => $checkout->getId(),
                        'products' => $checkout->getProducts(),
                    ]
                ),
                'text/html'
            )
        ;
        $this->get('mailer')->send($message);
    }

    public function removeFromCartAction($productId)
    {
        $session = $this->get('session');
        $cart = [];
        if ($session->has('cart')) {
            $cart = json_decode($session->get('cart'), true);
        }

        $product = $cart[$productId];

        unset($cart[$productId]);

        $session->set('cart', json_encode($cart));

        return new JsonResponse(
            [
                'status' => 'success',
                'data' => "Product {$product['title']} removed",
            ]
        );
    }
}
