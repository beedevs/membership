<?php

namespace SimpleMembershipBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PremiumProductController extends Controller
{
    public function indexAction()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $data = $entityManager->getRepository('SimpleMembershipBundle:Product')->getPremiumList();

        $userProducts = [];

        foreach ($this->getUser()->getProducts() as $product) {
            array_push($userProducts, $product->getId());
        }

        return $this->render(
            '@SimpleMembership/premium/index.html.twig',
            [
                'products' => $data,
                'userProducts' => $userProducts
            ]
        );
    }
}
