<?php

namespace SimpleMembershipBundle\Controller;

use SimpleMembershipBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Product controller.
 *
 */
class ProductController extends Controller
{
    /**
     * Lists all product entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('SimpleMembershipBundle:Product')->findAll();

        return $this->render(
            '@SimpleMembership/product/index.html.twig',
            array(
                'products' => $products,
            )
        );
    }

    /**
     * Creates a new product entity.
     *
     */
    public function newAction(Request $request)
    {
        $product = new Product();
        $form = $this->createForm('SimpleMembershipBundle\Form\ProductType', $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $this->filesUpload($product);
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('admin_product_show', ['productId' => $product->getId()]);
        }

        return $this->render(
            '@SimpleMembership/product/new.html.twig',
            array(
                'product' => $product,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * @param Product $product
     */
    private function filesUpload($product)
    {
        $image = $product->getThumbFile();

        if ($image) {
            $imageName = md5(uniqid()).'.'.$image->guessExtension();
            $path = $this->getParameter('media_folder');

            $image->move(
                $path.'/img/',
                $imageName
            );
            $product->setThumb($imageName);
        }
    }

    /**
     * Finds and displays a product entity.
     *
     */
    public function showAction($productId)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $product = $entityManager->getRepository('SimpleMembershipBundle:Product')->find($productId);

        if (!$product) {
            throw new NotFoundHttpException('Product not found');
        }

        return $this->render(
            '@SimpleMembership/product/show.html.twig',
            array(
                'product' => $product,
            )
        );
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     */
    public function editAction(Request $request, Product $product)
    {
        $editForm = $this->createForm('SimpleMembershipBundle\Form\ProductType', $product);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->filesUpload($product);

            $this->getDoctrine()->getManager()->persist($product);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_product_index');
        }

        return $this->render(
            '@SimpleMembership/product/edit.html.twig',
            array(
                'product' => $product,
                'form' => $editForm->createView(),
            )
        );
    }

    /**
     * Deletes a product entity.
     *
     */
    public function deleteAction(Product $product)
    {
        if (!$product) {
            throw $this->createNotFoundException('No product found');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($product);
        $em->flush();

        return new Response('ok');
    }
}
