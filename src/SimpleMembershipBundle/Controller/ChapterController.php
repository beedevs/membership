<?php

namespace SimpleMembershipBundle\Controller;

use SimpleMembershipBundle\Entity\Chapter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Chapter controller.
 *
 */
class ChapterController extends Controller
{
    /**
     * Lists all chapter entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $chapters = $em->getRepository('SimpleMembershipBundle:Chapter')->findAll();

        $form = $this->createForm('SimpleMembershipBundle\Form\ChapterType', null, array(
            'action' => $this->generateUrl('admin_chapter_new')
        ));

        return $this->render(
            '@SimpleMembership/chapter/index.html.twig',
            array(
                'chapters' => $chapters,
                'form' => $form->createView(),
            )
        );
    }

    public function newAction(Request $request) {
        $chapter = new Chapter();
        $form = $this->createForm('SimpleMembershipBundle\Form\ChapterType', $chapter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($chapter);
            $em->flush();

            return $this->redirectToRoute('admin_chapter_index');
        }
    }

    /**
     * Displays a form to edit an existing chapter entity.
     *
     */
    public function editAction(Request $request, Chapter $chapter)
    {
        $editForm = $this->createForm('SimpleMembershipBundle\Form\ChapterType', $chapter);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->persist($chapter);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_chapter_index');
        }

        return $this->render(
            '@SimpleMembership/chapter/edit.html.twig',
            array(
                'chapter' => $chapter,
                'form' => $editForm->createView()
            )
        );
    }

    /**
     * Deletes a chapter entity.
     *
     */
    public function deleteAction(Chapter $chapter)
    {

        if (!$chapter) {
            throw $this->createNotFoundException('No chapter found');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($chapter);
        $em->flush();

        return new Response('ok');
    }
}
