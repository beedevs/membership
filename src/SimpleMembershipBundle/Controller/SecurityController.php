<?php
/**
 * Created by PhpStorm.
 * User: moris
 * Date: 10/12/2016
 * Time: 14:27
 */

namespace SimpleMembershipBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\SecurityController as BaseSecurityController;

class SecurityController extends BaseSecurityController
{

    /**
     * Overriding login to add custom logic.
     */
    public function loginAction(Request $request)
    {
        if( $this->isGranted('IS_AUTHENTICATED_REMEMBERED') ){
            return new RedirectResponse($this->container->get('router')->generate('simple_membership_homepage', array()));
        }

        return parent::loginAction($request);
    }
}