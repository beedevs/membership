<?php
/**
 * Created by PhpStorm.
 * User: moris
 * Date: 4.2.2017
 * Time: 22:46
 */

namespace SimpleMembershipBundle\EventListener;

use Doctrine\ORM\EntityManager;

class BeforeRequestListener
{
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function onKernelRequest()
    {
    }
}