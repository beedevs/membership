<?php

namespace SimpleMembershipBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SimpleMembershipBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
