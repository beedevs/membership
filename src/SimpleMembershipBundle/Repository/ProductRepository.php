<?php

namespace SimpleMembershipBundle\Repository;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use SimpleMembershipBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * ProductRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProductRepository extends \Doctrine\ORM\EntityRepository
{
    public function getAvailable(User $user)
    {
        $query = $this->createQueryBuilder('p');
        $data = $query->select('p')
            ->leftJoin('p.users', 'u')
            ->where('u.id = :user')
            ->orWhere('p.isFree = :true')
            ->andWhere('p.isPublished = :published')
            ->setParameter('user', $user->getId())
            ->setParameter('true', true)
            ->setParameter('published', true)
            ->getQuery();

        return $data->getResult();
    }

    public function getPremiumList()
    {
        $query = $this->createQueryBuilder('p');
        $data = $query->select('p')
            ->leftJoin('p.users', 'u')
            ->where('p.isFree = :false')
            ->setParameter('false', false)
            ->orderBy('p.id', Criteria::ASC)
            ->getQuery();

        return $data->getResult(Query::HYDRATE_ARRAY);
    }

    public function getDistintcPriceTypes()
    {
        return [
            'true' => true,
            'false' => false,
        ];
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function preBuildQuery()
    {
        $query = $this->createQueryBuilder('p');
        $query->select('p')
            ->leftJoin('p.posts', 'posts')
            ->join('p.category', 'category')
            ->where('p.isPublished = :published')
            ->setParameter('published', true);

        return $query;
    }

    public function applyPriceFilter(QueryBuilder $query, Session $session, $filters)
    {
        if (!isset($filters['paid']) && !isset($filters['free'])) {
            $session->remove('isPaid');
            $session->remove('isFree');
        } elseif (isset($filters['paid']) && isset($filters['free'])) {
            $session->set('isPaid', true);
            $session->set('isFree', true);
        } elseif (isset($filters['paid'])) {
            $query->andWhere('p.isFree = :price');
            $query->setParameter('price', false);
            $session->set('isPaid', true);

            if ($session->has('isFree')) {
                $session->remove('isFree');
            }
        } elseif (isset($filters['free'])) {
            $query->andWhere('p.isFree = :price');
            $query->setParameter('price', true);
            $session->set('isFree', true);
            if ($session->has('isPaid')) {
                $session->remove('isPaid');
            }
        }
    }

    public function applyLevelFilter(QueryBuilder $query, Session $session, $filters)
    {
        $query->andWhere('p.level in (:levels)');
        $levels = [];
        foreach ($filters as $key => $filter) {
            $levels[] = $key;
        }
        $query->setParameter('levels', $levels);
        $session->set('level', $levels);
    }

    public function applyCategoryFilter(QueryBuilder $query, Session $session, $filters)
    {
        $query->andWhere('category.id in (:ids)');
        $ids = [];
        foreach ($filters as $key => $filter) {
            $ids[] = $key;
        }
        $query->setParameter('ids', $ids);
        $session->set('categories', $ids);
    }
}
