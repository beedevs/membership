<?php

namespace SimpleMembershipBundle\Repository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query;

/**
 * CategoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CategoryRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @return array
     */
    public function getCategoriesFilter()
    {
        if ($this->_em->getFilters()->has('categories')){
            $this->_em->getFilters()->disable('categories');
        }

        $query = $this->createQueryBuilder('c');

        $categories = $query->select('c.id, c.title')
            ->orderBy('c.title', Criteria::DESC)
            ->getQuery();

        return $categories->getResult(Query::HYDRATE_ARRAY);
    }
}
