<?php
/**
 * Created by PhpStorm.
 * User: moris
 * Date: 19/02/2017
 * Time: 22:47
 */

namespace SimpleMembershipBundle\Services;


use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class FiltersOnMain
{

    public function __construct(EntityManager $entityManager, Session $session)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
    }
    
    /**
     * @param Request $request
     * @param $query
     */
    public function setFilters(Request $request, $query)
    {
        $productRepository = $this->entityManager->getRepository('SimpleMembershipBundle:Product');
        $filters = $request->request->all();

        if (isset($filters['filters'])) {
            if (isset($filters['filters']['price'])) {
                $productRepository->applyPriceFilter($query, $this->session, $filters['filters']['price']);
            } else {
                $this->session->remove('isFree');
                $this->session->remove('isPaid');
            }

            if (isset($filters['filters']['level'])) {
                $productRepository->applyLevelFilter($query, $this->session, $filters['filters']['level']);
            } else {
                $this->session->remove('level');
            }

            if (isset($filters['filters']['categories'])) {
                $productRepository->applyCategoryFilter($query, $this->session, $filters['filters']['categories']);
            } else {
                $this->session->remove('categories');
            }
        } else {
            $this->session->remove('isFree');
            $this->session->remove('isPaid');
            $this->session->remove('level');
            $this->session->remove('categories');
        }
    }
}