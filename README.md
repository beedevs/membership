
#Symfony 3 Simple membership project

 - Standard SYmfony 3
     
 - used bundles:
    - required:
         - `symfony/assetic-bundle:^2.8`
         - `twitter/bootstrap:3.*`
         - `components/jquery:1.11.1`
         - `friendsofsymfony/jsrouting-bundle:^1.6`
         - `twig/extensions:^1.4`
    - required-dev:
         - `deployer/deployer:^4.0`
         
 - Static files located `src/SimpleMembershipBundle/Resources/views/static/`
    If you would like to use same themplate with other page leave it as is,
    Please follow annotations in templates
    
    Or clean file to use your own design
    
 - In this project already installed deployer, you can read more about this tools on deployer.org
    
