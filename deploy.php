<?php

require 'recipe/symfony3.php';

server('prod', '37.247.116.123')
    ->user('root')
    ->forwardAgent()
    ->env('deploy_path', '/var/www/simplemember');

set('keep_releases', 3);
set('writable_mode', 'chown');
set('writable_use_sudo', false);

task('deploy:writable', function(){
    cd('{{release_path}}');
    run('chown -R www-data:www-data var/logs var/cache var/sessions');
    run('chown -R www-data:www-data web/uploads/img web/uploads/pdf');
});

task(
    'database:schema:update',
    function () {
        if (askConfirmation('Update doctrine schema?')) {
            run(
                '{{bin/php}} {{release_path}}/'.trim(
                    get('bin_dir'),
                    '/'
                ).'/console doctrine:schema:update --force --env={{env}} --no-debug --no-interaction'
            );
        }
    }
);

task('deploy:assetic:dump', function(){
    cd('{{release_path}}');
    run('php bin/console --env=prod assetic:dump');
});

task('deploy:add:admin', function(){
    if (askConfirmation('Add admin user?')) {
        cd('{{release_path}}');
        run('php bin/console --env=prod fos:user:create admin admin@admin.com admin --super-admin');
    }
});


set('shared_files', [
    'app/config/parameters.yml'
]);

set('shared_dirs', [
    'web/uploads/img',
    'web/uploads/pdf',
]);

set('writable_dirs', []);

task(
    'cache:clear',
    function () {
        cd('{{release_path}}');
        run('php bin/console --env=prod cache:clear');
    }
);

before('deploy:symlink', 'database:schema:update');
before('success', 'deploy:assetic:dump');
after('success', 'deploy:add:admin');
set('repository', 'git@bitbucket.org:beedevs_team/membership.git');